# go

Go helper packages

## Install

- Install to your local

  ```sh
  go get -u bitbucket.org/absoluteyl/go
  ```

- Import to your project

  ```go
  import "bitbucket.org/absoluteyl/go"
  ```

## Packages

### absoluthttp

- GetLogLevel:

  Automatically set log level by HTTP response code, for example:

  ```go
  absoluthttp.GetLogLevel(http.StatusOK) // INFO level
  absoluthttp.GetLogLevel(http.StatusNotFound) // ERROR level
  ```

### absolutos

Enhancement of go official os package, currently supports following method:

- Getenv:

  Enhance Getenv method of official os package with a default value, for example:

  ```go
  absolutos.Getenv("PORT", "8080")
  ```
