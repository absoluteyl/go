package absolutos

import "os"

// Getenv Enhance Getenv method of official os package with a default value
func Getenv(key, defaultValue string) (value string) {
	value = os.Getenv(key)
	if len(value) == 0 {
		value = defaultValue
	}
	return
}
